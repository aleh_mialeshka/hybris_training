package trainingfacade.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import trainingfacade.layers.service.MyProductService;

/**
 * Cronjob that print number of products with not empty additionalDescription to console.
 */
public class ProductWriterJob extends AbstractJobPerformable<CronJobModel> {
    private MyProductService myProductService;
    private static final Logger LOGGER = Logger.getLogger(ProductWriterJob.class);

    @Override
    public PerformResult perform(CronJobModel cronJobModel) {
        int numberOfProductsWithNotEmptyAdditionalDescription = myProductService.findAllProductWithAdditionalDescription().size();
        final String message = "Number of products with not empty additionalDescription = %d";
        LOGGER.info(String.format(message, numberOfProductsWithNotEmptyAdditionalDescription));
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public void setMyProductService(MyProductService myProductService) {
        this.myProductService = myProductService;
    }
}
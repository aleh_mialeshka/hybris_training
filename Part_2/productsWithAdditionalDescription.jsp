<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Products</title>
</head>
<body>
    Products with additional description -----------><br>
    <c:forEach var="product" items="${products}">
        <p>
            ${product.additionalDescription}
        </p>
    </c:forEach>
</body>
</html>
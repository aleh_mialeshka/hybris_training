package trainingfacade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import trainingfacade.layers.facade.MyProductFacade;

/**
 * Test controller for products with not empty additionalDescription.
 */
@Controller
public class TestController {
    @Autowired
    private MyProductFacade myProductFacade;

    @GetMapping(value = "/myProducts")
    public ModelAndView getProducts(ModelAndView modelAndView) {
        final String viewName = "productsWithAdditionalDescription";
        final String productsWithAdditionalDescription = "products";
        modelAndView.setViewName(viewName);
        modelAndView.addObject(productsWithAdditionalDescription, myProductFacade.findAllProductWithAdditionalDescription());
        return modelAndView;
    }
}
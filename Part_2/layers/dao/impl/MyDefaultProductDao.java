package trainingfacade.layers.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import trainingfacade.layers.dao.MyProductDao;
import java.util.List;

/**
 * Main implementation of dao interface.
 */
public class MyDefaultProductDao implements MyProductDao {
    private FlexibleSearchService flexibleSearchService;

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    @Override
    public List<ProductModel> findAllProductWithAdditionalDescription() {
        final String queryString = "SELECT {p:" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + " AS p} WHERE {p:" + ProductModel.ADDITIONALDESCRIPTION + "} IS NOT NULL";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        query.setDisableSearchRestrictions(true);   // i have problems without this line (session.catalogVersion)... :(
        return flexibleSearchService.<ProductModel>search(query).getResult();
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }
}
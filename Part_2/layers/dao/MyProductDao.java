package trainingfacade.layers.dao;

import de.hybris.platform.core.model.product.ProductModel;
import java.util.List;

/**
 * Main dao interface.
 */
public interface MyProductDao {

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    List<ProductModel> findAllProductWithAdditionalDescription();
}
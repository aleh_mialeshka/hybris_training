package trainingfacade.layers.facade;

import trainingfacade.data.ProductData;
import java.util.List;

/**
 * Main facade interface.
 */
public interface MyProductFacade {

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    List<ProductData> findAllProductWithAdditionalDescription();
}
package trainingfacade.layers.facade.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import trainingfacade.data.ProductData;
import trainingfacade.layers.facade.MyProductFacade;
import trainingfacade.layers.service.MyProductService;
import java.util.List;

/**
 * Main implementation of facade interface.
 */
public class MyDefaultProductFacade implements MyProductFacade {
    private MyProductService myProductService;
    private Converter<ProductModel, ProductData> converter;

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    @Override
    public List<ProductData> findAllProductWithAdditionalDescription() {
        List<ProductModel> productModels = myProductService.findAllProductWithAdditionalDescription();
        return converter.convertAll(productModels);
    }

    public void setMyProductService(MyProductService myProductService) {
        this.myProductService = myProductService;
    }

    public void setConverter(Converter<ProductModel, ProductData> converter) {
        this.converter = converter;
    }
}
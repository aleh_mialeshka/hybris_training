package trainingfacade.layers.service;

import de.hybris.platform.core.model.product.ProductModel;
import java.util.List;

/**
 * Main service interface.
 */
public interface MyProductService {

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    List<ProductModel> findAllProductWithAdditionalDescription();
}
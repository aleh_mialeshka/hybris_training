package trainingfacade.layers.service.impl;

import de.hybris.platform.core.model.product.ProductModel;
import trainingfacade.layers.dao.MyProductDao;
import trainingfacade.layers.service.MyProductService;
import java.util.List;

/**
 * Main implementation of service interface.
 */
public class MyDefaultProductService implements MyProductService {
    private MyProductDao myProductDao;

    /**
     * Find all products that have not empty field 'additionalDescription'.
     * @return list with these products.
     */
    @Override
    public List<ProductModel> findAllProductWithAdditionalDescription() {
        return myProductDao.findAllProductWithAdditionalDescription();
    }

    public void setMyProductDao(MyProductDao myProductDao) {
        this.myProductDao = myProductDao;
    }
}
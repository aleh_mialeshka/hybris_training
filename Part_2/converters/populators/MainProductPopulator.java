package trainingfacade.converters.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import trainingfacade.data.ProductData;

/**
 * Product populator for fields: name, code, description.
 */
public class MainProductPopulator implements Populator<ProductModel, ProductData> {

    /**
     * Populate name, code and description.
     * @param productModel - source.
     * @param productData = target.
     */
    @Override
    public void populate(ProductModel productModel, ProductData productData) throws ConversionException {
        productData.setName(productModel.getName());
        productData.setCode(productModel.getCode());
        productData.setDescription(productModel.getDescription());
    }
}
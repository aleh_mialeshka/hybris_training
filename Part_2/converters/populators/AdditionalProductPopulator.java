package trainingfacade.converters.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import trainingfacade.data.ProductData;

/**
 * Product populator for field: additionalDescription.
 */
public class AdditionalProductPopulator implements Populator<ProductModel, ProductData> {

    /**
     * Populate additionalDescription.
     * @param productModel - source.
     * @param productData = target.
     */
    @Override
    public void populate(ProductModel productModel, ProductData productData) throws ConversionException {
        final String code = productModel.getCode();
        final String additionalDescription = productModel.getAdditionalDescription();
        productData.setAdditionalDescription(String.format("%s:%s", code, additionalDescription));
    }
}
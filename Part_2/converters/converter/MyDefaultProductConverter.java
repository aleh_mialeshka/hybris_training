package trainingfacade.converters.converter;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import trainingfacade.data.ProductData;

import java.util.List;

/**
 * Converter from ProductModel to ProductData.
 */
public class MyDefaultProductConverter implements Converter<ProductModel, ProductData> {
    private List<Populator<ProductModel, ProductData>> populators;

    /**
     * Convert model to dto.
     * @param productModel current product model.
     * @return ready to use dto.
     */
    @Override
    public ProductData convert(ProductModel productModel) {
        ProductData productData = new ProductData();
        return convert(productModel, productData);
    }

    /**
     * Convert model to dto using populators.
     * @param productModel - source.
     * @param productData - target.
     * @return ready to use dto.
     */
    @Override
    public ProductData convert(ProductModel productModel, ProductData productData) throws ConversionException {
        populators.forEach(populator-> populator.populate(productModel, productData));
        return productData;
    }

    public void setPopulators(List<Populator<ProductModel, ProductData>> populators) {
        this.populators = populators;
    }
}
package trainingcore.handlers;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.attribute.DynamicLocalizedAttributeHandler;
import java.util.Locale;

/**
 * Dynamic attribute handler for customer customerDescription field.
 */
public class CustomerDescriptionHandler implements DynamicLocalizedAttributeHandler<String, CustomerModel> {
    private I18NService i18NService;

    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }

    /**
     * Create dynamic attribute with localisation.
     * @param customerModel current customer model.
     * @return <Name>: <Email>. Order quantity is <size of "Orders" collection field>.
     */
    @Override
    public String get(CustomerModel customerModel) {
        final String CUSTOMER_NAME = customerModel.getDisplayName(i18NService.getCurrentLocale());
        return getResultString(customerModel, CUSTOMER_NAME);
    }

    /**
     * Setter set to false.
     */
    @Override
    public void set(CustomerModel model, String s) {
        throw new UnsupportedOperationException();
    }

    /**
     * Create dynamic attribute with localisation.
     * @param customerModel current customer model.
     * @return <Name>: <Email>. Order quantity is <size of "Orders" collection field>.
     */
    @Override
    public String get(CustomerModel customerModel, Locale locale) {
        final String CUSTOMER_NAME = customerModel.getDisplayName(locale);
        return getResultString(customerModel, CUSTOMER_NAME);
    }

    /**
     * Setter set to false.
     */
    @Override
    public void set(CustomerModel model, String s, Locale locale) {
        throw new UnsupportedOperationException();
    }

    /**
     * Help method.
     * @param customerModel current customer model.
     * @param localizedName name after using localization.
     * @return <Name>: <Email>. Order quantity is <size of "Orders" collection field>.
     */
    private String getResultString(CustomerModel customerModel, String localizedName) {
        final String CUSTOMER_EMAIL = customerModel.getEmail();
        final int ORDER_SIZE = customerModel.getOrders().size();
        return String.format("%s: %s. Order quantity is %d.",localizedName, CUSTOMER_EMAIL, ORDER_SIZE);
    }
}
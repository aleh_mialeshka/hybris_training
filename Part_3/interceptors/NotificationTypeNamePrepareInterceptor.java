package trainingbackoffice.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import trainingbackoffice.workflow.WorkflowExecutor;
import trainingcore.model.NotificationTypeModel;

/**
 * Interceptor for NotificationType.
 */
public class NotificationTypeNamePrepareInterceptor implements PrepareInterceptor {
    private FlexibleSearchService flexibleSearchService;
    private WorkflowExecutor workflowExecutor;

    @Override
    public void onPrepare(Object object, InterceptorContext interceptorContext) {

        if (object instanceof NotificationTypeModel) {
            NotificationTypeModel notificationTypeModelToIntercept = (NotificationTypeModel) object;

            if(isModelExistsInDataBase(notificationTypeModelToIntercept)) {
                Long code = notificationTypeModelToIntercept.getCode();
                NotificationTypeModel existedNotificationTypeModelInDatabase = getNotificationTypeModelFromDatabase(code);

                /* increase frequency if model names are different */
                increaseFrequency(notificationTypeModelToIntercept, existedNotificationTypeModelInDatabase);
            }
        }
    }

    /**
     * Compare model names.
     * @param modelToIntercept new model.
     * @param modelInDataBase existed model.
     * @return {@code true} if names are different, otherwise {@code false}
     */
    private boolean areModelNamesDifferent(NotificationTypeModel modelToIntercept, NotificationTypeModel modelInDataBase) {
        return (!modelToIntercept.getName().equals(modelInDataBase.getName()));
    }

    /**
     * Increase model frequency if necessary.
     * @param notificationTypeModelToIntercept new model.
     * @param existedNotificationTypeModelInDatabase existed model.
     */
    private void increaseFrequency(NotificationTypeModel notificationTypeModelToIntercept, NotificationTypeModel existedNotificationTypeModelInDatabase) {
        if (areModelNamesDifferent(notificationTypeModelToIntercept, existedNotificationTypeModelInDatabase)) {
            Byte frequency = notificationTypeModelToIntercept.getFrequency();
            frequency = frequency == null ? 1 : ++frequency;
            notificationTypeModelToIntercept.setFrequency(frequency);
            workflowExecutor.startWorkflow(notificationTypeModelToIntercept);   // after frequency changing workflow starts
        }
    }

    /**
     * Check if model exists in database.
     * @param notificationTypeModel current model.
     * @return {@code true} if exists, otherwise {@code false}
     */
    private boolean isModelExistsInDataBase(NotificationTypeModel notificationTypeModel) {
        final String query = "Select {%s} FROM {%s} WHERE {code}=%d";
        final String pk = NotificationTypeModel.PK;
        final String type = NotificationTypeModel._TYPECODE;
        final Long code = notificationTypeModel.getCode();
        return flexibleSearchService.search(String.format(query, pk, type, code)).getResult().size() > 0;
    }

    /**
     * Get model from database using model code.
     * @param code model code.
     * @return an instance of NotificationType model from database.
     */
    private NotificationTypeModel getNotificationTypeModelFromDatabase(Long code) {
        NotificationTypeModel template = new NotificationTypeModel();
        template.setCode(code);
        return flexibleSearchService.getModelByExample(template);
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public void setWorkflowExecutor(WorkflowExecutor workflowExecutor) {
        this.workflowExecutor = workflowExecutor;
    }
}
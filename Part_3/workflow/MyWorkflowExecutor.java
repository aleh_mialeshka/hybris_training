package trainingbackoffice.workflow;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import trainingcore.model.NotificationTypeModel;

/**
 * Workflow executor implementation.
 */
public class MyWorkflowExecutor implements WorkflowExecutor{
    private WorkflowService workflowService;
    private WorkflowTemplateService workflowTemplateService;
    private WorkflowProcessingService workflowProcessingService;
    private UserService userService;
    private ModelService modelService;

    /**
     * Start workflow.
     * @param notificationTypeModel model in which the frequency was changed.
     */
    public void startWorkflow(NotificationTypeModel notificationTypeModel) {
        final String workflowCode = "NewNotificationFrequency";
        final WorkflowTemplateModel workflowTemplate = workflowTemplateService.getWorkflowTemplateForCode(workflowCode);
        final WorkflowModel workflow = workflowService.createWorkflow(workflowTemplate, notificationTypeModel, userService.getAdminUser());

        modelService.save(workflow);
        workflow.getActions().forEach(action -> modelService.save(action));
        workflowProcessingService.startWorkflow(workflow);     // GO GO GO!!!
    }

    public void setWorkflowService(WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    public void setWorkflowTemplateService(WorkflowTemplateService workflowTemplateService) {
        this.workflowTemplateService = workflowTemplateService;
    }

    public void setWorkflowProcessingService(WorkflowProcessingService workflowProcessingService) {
        this.workflowProcessingService = workflowProcessingService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
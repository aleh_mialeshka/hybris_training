package trainingbackoffice.workflow;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.workflow.jobs.AutomatedWorkflowTemplateJob;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowDecisionModel;
import org.apache.log4j.Logger;
import trainingcore.model.NotificationTypeModel;
import java.util.List;

/**
 * Automated workflow which is invoked after a frequency changing.
 */
public class FrequencyWorkflow implements AutomatedWorkflowTemplateJob {
    private static final Logger LOGGER = Logger.getLogger(FrequencyWorkflow.class);

    /**
     * Perform a workflow.
     * @param workflowActionModel current workflow action model.
     * @return result of the workflow
     */
    @Override
    public WorkflowDecisionModel perform(WorkflowActionModel workflowActionModel) {
        NotificationTypeModel notificationTypeModel = getAttachedNotificationType(workflowActionModel);
        LOGGER.info(notificationTypeModel.getFrequency());
        for (WorkflowDecisionModel workflowDecisionModel : workflowActionModel.getDecisions()) {
            return workflowDecisionModel;
        }
        return null;
    }

    /**
     * Get attached NotificationType model from workflow action model.
     * @param workflowActionModel current workflow action model.
     * @return an instance of NotificationType.
     */
    private NotificationTypeModel getAttachedNotificationType(WorkflowActionModel workflowActionModel) {
        List<ItemModel> attachments = workflowActionModel.getAttachmentItems();
        if (attachments != null) {
            for (ItemModel itemModel : attachments) {
                if (itemModel instanceof NotificationTypeModel) {
                    return  (NotificationTypeModel) itemModel;
                }
            }
        }
        return null;
    }
}
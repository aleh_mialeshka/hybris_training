package trainingbackoffice.workflow;

import trainingcore.model.NotificationTypeModel;

/**
 * Workflow interface.
 */
public interface WorkflowExecutor {
    void startWorkflow(NotificationTypeModel notificationTypeModel);
}
package trainingbackoffice.restrictions.productRestriction;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.SearchRestrictionModel;
import trainingbackoffice.restrictions.AbstractRestrictionGenerator;

/**
 * Add restriction for products.
 */
public class MyDescriptionCatalogProductRestrictionGenerator extends AbstractRestrictionGenerator implements ProductRestrictionGenerator {

    /**
     * Add restriction for anonymous, he can see only products with not null additionalDescription in russian from Online catalog.
     */
    @Override
    public void addProductRestriction() {
        final String restrictionTarget = "anonymous";
        final String restrictionCode = "productRestriction_test";
        final String restrictionQuery = "{additionalDescription[ru]} IS NOT NULL AND " +
                                        "{catalogVersion} IN " +
                                        "({{SELECT {pk} FROM {CatalogVersion} " +
                                        "WHERE {version}='Online'}})";

        final ComposedTypeModel restrictedType = typeService.getComposedTypeForClass(ProductModel.class);
        final PrincipalModel principal = userService.getUserForUID(restrictionTarget);
        final SearchRestrictionModel searchRestriction = modelService.create(SearchRestrictionModel.class);

        searchRestriction.setCode(restrictionCode);
        searchRestriction.setActive(Boolean.TRUE);
        searchRestriction.setQuery(restrictionQuery);
        searchRestriction.setRestrictedType(restrictedType);
        searchRestriction.setPrincipal(principal);
        searchRestriction.setGenerate(Boolean.TRUE);
        modelService.save(searchRestriction);
    }
}
/*
SELECT {pk} FROM {Product}
WHERE {additionalDescription[ru]} IS NOT NULL
AND {catalogVersion} IN
	({{SELECT {pk} FROM {CatalogVersion}
     WHERE {version}='Online'}})
*/
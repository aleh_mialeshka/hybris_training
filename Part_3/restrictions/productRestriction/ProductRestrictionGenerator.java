package trainingbackoffice.restrictions.productRestriction;

/**
 * Product restriction interface.
 */
public interface ProductRestrictionGenerator {

    /**
     * Add ptoduct restriction.
     */
    void addProductRestriction();
}
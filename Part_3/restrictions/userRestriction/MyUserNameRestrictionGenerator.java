package trainingbackoffice.restrictions.userRestriction;

import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.SearchRestrictionModel;
import de.hybris.platform.core.model.user.UserModel;
import trainingbackoffice.restrictions.AbstractRestrictionGenerator;

/**
 * Add restriction for users.
 */
public class MyUserNameRestrictionGenerator extends AbstractRestrictionGenerator implements NameRestrictionGenerator {

    /**
     * Add restriction for anonymous, he can see only users, which names consist of  "mana" or "port" ignoring registry.
     */
    public void addNameRestriction() {
        final String restrictionTarget = "anonymous";
        final String restrictionCode = "nameRestriction_test";
        final String restrictionQuery = "UPPER({name}) LIKE UPPER('%mana%') OR UPPER({name}) LIKE UPPER('%port%')";

        final ComposedTypeModel restrictedType = typeService.getComposedTypeForClass(UserModel.class);
        final PrincipalModel principal = userService.getUserForUID(restrictionTarget);
        final SearchRestrictionModel searchRestriction = modelService.create(SearchRestrictionModel.class);

        searchRestriction.setCode(restrictionCode);
        searchRestriction.setActive(Boolean.TRUE);
        searchRestriction.setQuery(restrictionQuery);
        searchRestriction.setRestrictedType(restrictedType);
        searchRestriction.setPrincipal(principal);
        searchRestriction.setGenerate(Boolean.TRUE);
        modelService.save(searchRestriction);
    }
}

/*
SELECT {pk}
FROM {User}
WHERE UPPER({name}) LIKE UPPER('%mana%') OR UPPER({name}) LIKE UPPER('%port%')
 */
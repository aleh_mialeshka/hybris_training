package trainingbackoffice.restrictions.userRestriction;

/**
 * Name restriction interface.
 */
public interface NameRestrictionGenerator {

    /**
     * Add name restriction.
     */
    void addNameRestriction();
}
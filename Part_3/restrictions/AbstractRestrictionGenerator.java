package trainingbackoffice.restrictions;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;

/**
 * Abstract class with all needed fields.
 */
public abstract class AbstractRestrictionGenerator {
    protected ModelService modelService;
    protected FlexibleSearchService flexibleSearchService;
    protected TypeService typeService;
    protected UserService userService;

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}